#!/bin/bash

# **Important** VAULT_ADDR, VAULT_TOKEN, GOOGLE_PROJECT, and GOOGLE_CREDENTIALS_PATH environment variables should be set prior to running this script
# This script configures the GCP secrets Engine in Vault
# We assume the workspace name is gitlab-ci-gcp, please adjust this if needed

echo "Please ensure VAULT_ADDR, VAULT_TOKEN, GOOGLE_PROJECT and GOOGLE_CREDENTIALS_PATH environment variables are set."
./check_vault_vars.sh
if [ ! -f "vars_are_valid" ]; then
    echo "ERR: Failed to verify required Variables."
    exit 1
fi

echo "INFO: Checking connectivity to vault server: ${VAULT_ADDR}, and vault token"
curl -kv "${VAULT_ADDR}/v1/sys/health"
vault status
vault token lookup

echo "INFO: Creating secrets engine"
# Create the gcp secrets engine to issue credentials that are valid for five minutes  
vault secrets enable gcp
vault write gcp/config credentials=@${GOOGLE_CREDENTIALS_PATH}
vault secrets tune -default-lease-ttl=300s -max-lease-ttl=600s gcp

echo "INFO: Creating roleset binding"
# Create role set bindings for the dynamically generated credentials
vault write gcp/roleset/gitlab \
    project="${GOOGLE_PROJECT}" \
    secret_type="service_account_key"  \
    bindings=-<<EOF
resource "//cloudresourcemanager.googleapis.com/projects/${GOOGLE_PROJECT}" {
  roles = ["roles/editor"]
}
EOF

echo "INFO: Creating policy for GitLab Runner"
# Create a policy for GitLab runners
vault policy write gitlab-runner -<<EOH
path "gcp/key/gitlab" {
  capabilities = ["read"]
}
EOH

echo "INFO: Creating token for GitLab Runner"
# Create a token for the Runners with a TTL of 24 hours
vault token create -policy=gitlab-runner -ttl=24h 

echo "INFO: script completed. Please configure the above token as a VAULT_TOKEN variable in GitLab CI/CD Variables"
echo "To test: vault read -format=json gcp/key/gitlab"